package com.zuitt.batch193;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Factorial {

    public static void main(String[] args){
        int x = 2;


        int factorial = 1;

//        if(number < 0){
//            System.out.println("Cant");
//        } else {
//
//            for (int num = 1; num <= number; num++){
//
//               factorial = factorial * num;
//            }
//
//            System.out.println(factorial);
//        }

        Scanner appScanner = new Scanner(System.in);

        int number = 0;

        try{
            System.out.println("Input an integer whose factorial will be computed");
            number = appScanner.nextInt();
        }  catch (InputMismatchException e){
            System.out.println("Input is not a number");
        }   catch (Exception e) {
            System.out.println("Invalid input");
        }  finally {
            while(x <= number){
                factorial = factorial * x;
                x++;
            }
            System.out.println("The factorial of " + number + " is " + factorial);
        }
    }
}
